FROM node
COPY build/ /frontend/
RUN npm install -g serve
CMD ["serve", "-s", "frontend"]
EXPOSE 5000