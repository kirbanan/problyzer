import React from 'react';
// import { Card } from 'material-ui';
import { Card, CardContent } from '@material-ui/core';

export class Claim extends React.Component{
    constructor(props) {
        super(props);

        this.id=props.id;
        this.text=props.text;
        this.category=props.category;
        this.status=props.status;
        this.vkLink=props.vkLink;
    }
    render(){
        return(
        <Card>
            <CardContent className="claim__container" key={this.id}>
                <h4 className="claim__heading">Claim #{this.id}</h4>
                <p className="claim__text">
                    {this.text}
                </p>
                <span>Category: {this.category}</span>
                <br />
                {
                    (this.status==='COMPLETE') ? 
                        <a href={this.vkLink}>Look in VK</a> 
                    :
                        <span>Current status: {this.status}</span>
                }
            </CardContent>
        </Card>
        )
    }
}