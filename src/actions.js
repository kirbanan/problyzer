import axios from 'axios';

const serverUrl = `http://${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}`;
const apiPrefix = "/api/v1";
const clientId = "ui";
var accessToken;
function saveAccessToken(response){
    return accessToken = response.data.access_token;
}

export function register(){
    // if (accessToken === undefined) {
        return axios
                .post(`${serverUrl}/singup?client_id=${clientId}`, null, {
                    withCredentials: true
                })
                .then(saveAccessToken)
                .catch(reason => console.log(reason));
    // }
    // else {
        // return new Promise((resolve, reject)=>{
            // reject('already registered');
        // });
    // }
}

export function login() {
    if (accessToken === undefined) {
        console.log('accessToken in login:', accessToken);
        return register()
                .then(res => axios
                            .post(`${serverUrl}/singin?client_id=${clientId}`, null, {
                                withCredentials: true
                            })
                            .then(saveAccessToken)
                            .catch(reason => console.log(reason))    
        )
    } else {
        return axios
                .post(`${serverUrl}/singin?client_id=${clientId}`, null, {
                    withCredentials: true
                })
                .then(res => res)
                .catch(reason => console.log(reason))
    }
}

export function logout() {
    accessToken = undefined;
    axios
        .get(`${serverUrl}/logout`, {
            withCredentials: true
        })
        .then(response => console.log(response))
        .catch(reason => console.log(reason));
}

// document.getElementById("external-user-info-btn").addEventListener("click", e => {
//     axios
//         .get(`${serverUrl}/me`, {
//             withCredentials: true
//         })
//         .then(response => console.log(response))
//         .catch(reason => console.log(reason));
// });

export function createClaim(text) {
    return axios
        .post(`${serverUrl}${apiPrefix}/claim-service/claim?text=${text}`, null, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            },
        })
        .then(response => response)
        .catch(reason => console.log(reason));
}

export function getClaims(){
    // if (typeof(accessToken) !== 'undefined') {
    //     return axios
    //             .get(`${serverUrl}${apiPrefix}/claim-service/claim/my`, {
    //                 headers: {
    //                     Authorization: `Bearer ${accessToken}`
    //                 },
    //             })
    //             .then(response => response.data)
    //             .catch(reason => console.log(reason))
    // } 
    // else {
        if(typeof(accessToken) !== 'undefined') {

        
        return login().then(accessToken => {
            console.log('at in getClaims(): ',accessToken.data.access_token);
            return axios
            .get(`${serverUrl}${apiPrefix}/claim-service/claim/my`, {
                headers: {
                    Authorization: `Bearer ${accessToken.data.access_token}`
                },
            })
            .then(response => response.data)
            .catch(reason => console.log(reason))
        })
        }
        // return new Promise((resolve, reject) => {
        //     reject('getClaims(): accessToken undefined')
        // })
    // }
};

export function sendClaimFile(file){
    let data = new FormData();
    data.set('file', file);
    return axios
        .post(`${serverUrl}${apiPrefix}/claim-service/claim`, data, {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        })
        .then(response => console.log(response))
        .catch(reason => console.log(reason));
}