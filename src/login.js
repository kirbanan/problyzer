import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { Button } from '@material-ui/core'
const styles = {
    card: {
      margin: "0 15rem",
      textAlign: "center"
    },
  };

export default class Login extends React.Component {
  render() {
    return (
    //   <div>
    //     <p>Login</p>
    //     <button onClick={this.props.login}>
    //       {!this.props.isLogged ? "Log In" : "Log Out"}
    //     </button>
    //   </div>
    <div className="wrapper">
        <Card className="loginCard" style={styles.card}>
          <CardContent className="loginCard__content">
            
            <form id="formLogIn" >
                {!this.props.isLogged ? (
                    <div>
                        <Typography className="loginCard__head" variant="h5">
                        Welcome to our service!
                        </Typography>
                        <Typography
                        className="loginCard__head"
                        color="textSecondary"
                        gutterBottom
                        >
                        Please log in to continue
                        </Typography>
                        <Button
                            className="logIn__button"
                            variant="contained"
                            color="secondary"
                            onClick={
                                this.props.login.bind(this, "google")
                            }
                        >
                            Google Auth
                        </Button>
                        <Button
                        className="logIn__button"
                        variant="contained"
                        color="primary"
                        onClick={
                            this.props.login.bind(this, "facebook")
                        }
                        >
                        Facebook Auth
                        </Button>
                    </div>
                ) : 
                <Button
                    onClick={this.props.login.bind(this, "google")}
                >
                    Log Out
                </Button>
                }
            </form>
          </CardContent>
        </Card>
      </div>
    );
  }
}
