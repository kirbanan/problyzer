import React from "react";
import { StyledDropZone } from "react-drop-zone";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Icon from "@material-ui/core/Icon";
import "react-drop-zone/dist/styles.css";
import { getClaims, createClaim, register, login as logIn, sendClaimFile } from './actions';
import { Claim } from "./claim";

const styles = {
    card: {
      margin: "0 15rem",
      textAlign: "center"
    },
};

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: {},
      claims: {},
      claimsHTML: {},
    };
  }

  addFile = (file) => {
    this.setState({file: file, });
  };
  async sendClaim(text){
    const res = await createClaim(text).then(res => res);
    // if (res.status === 200) {
    // }
    console.log('res in createClaim :', res);
  }
  async get_claims(){
    const claims = await logIn()
    .then(accessToken => console.log("home AT: ", accessToken))
    .then(accessToken => getClaims())
    .then(res => res)

    if(typeof(claims)!=="undefined"){this.setState({claims: claims})}
  }
  componentWillMount() {
    this.get_claims()
  }
  componentDidMount() {
      
      document.addEventListener("click", e => {
        let claimText = document.querySelector("#text"),
            claimSendBtn = document.querySelector("#sendClaimText"),
            claimSendFile = document.querySelector("#sendFile");

        switch(e.target){
            case claimSendBtn:
                this.sendClaim(claimText.value);
                claimText.value = "";
                // this.setState({claims: claimText.value});
                break;
            case claimSendFile:
                console.log('send');
                sendClaimFile(this.state.file)
                  .then(response => console.log(response));
                break;
            default:
                return e.target;
        }
      })
  }

  render() {
    //TODO: checking type of input file
    return (
        <div className="wrapper">
        <div className="homeCard" style={styles.card}>
        <Card>
            <h1>Here are your claims</h1>
          <CardContent className="homeCard__content">
            {
              (typeof(this.state.claims.totalCount)==='undefined') ? ("Loading...") : (
                (this.state.claims.totalCount === 0) ? ("You have no claims yet") : (
                  
                  
                  Object.values(this.state.claims.content).map(claim => {
                              return (<Claim 
                                          id={claim.id} 
                                          text={claim.text} 
                                          category={claim.category} 
                                          status={claim.status} 
                                          vkLink={claim.vkLink} 
                                        />)
                        })
                )
              )
            }
            {console.log(this.state)}
            <h2>Create new claim</h2>
            {/* <form id="sendTextForm" action="/" method="get"> */}
                <textarea id="text" placeholder="type your claim here ..."></textarea>
                <br/>
                <button type="submit" id="sendClaimText">Send</button>
            {/* </form> */}
            
            <h2>Send file with claim</h2>
            <form id="formSendFile" method="post">
            <StyledDropZone id="input" onDrop={this.addFile} />
            <div className="file-name"> {this.state.file.name} </div>

            <Button id="sendFile" variant="contained" color="secondary" aria-label="Add">
            <Icon>add_icon</Icon>
            Send File
            </Button>
            </form>
          </CardContent>
        </Card>
      </div>
      </div>

        

    );
  }
}
