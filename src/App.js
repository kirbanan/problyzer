import React from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import PrivateRoute from "react-private-route";
import axios from 'axios';

import Login from "./login";
import Home from "./home";
import NotFound from "./notfound";
import { register, login as logIn, logout } from './actions';

import "./index.css";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      isLoggedIn: false
    };
    this.mystorage = window.sessionStorage;
    window.onclose = function(){
      logout()
    }
    if (this.mystorage.getItem("isLoggedIn") === null){
      this.mystorage.setItem("isLoggedIn", false);
    }
    this.isLoggedIn = this.isLoggedIn.bind(this);
    this.login = this.login.bind(this);
    console.log('sessionStorage.getItem("isLoggedIn") :', this.mystorage.getItem('isLoggedIn')[0]);
  }
  isLoggedIn() {
    if (this.mystorage.getItem('isLoggedIn')[0]==='t'){
      return true
    }
    return false;
  }
  async login(provider) {
    var data = this.mystorage.getItem('isLoggedIn');
    
    if (data[0]==='t'){
      this.setState({ isLoggedIn: !this.state.isLoggedIn });
      axios
          .get(`http://${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/logout`, {
              withCredentials: true
          })
          .then(response => {console.log(response);})
          .catch(reason => console.log(reason));
      this.mystorage.setItem('isLoggedIn', false);
      window.location.href = "/"
    } else if (data[0]==='f') {
      let serverUrl = `http://${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}`;
      const redirectUri = `${window.location.origin}/`; //CHANGE ?
      const authUri = `${serverUrl}/singin/oauth2/${provider}?redirect_uri=${redirectUri}`;
      register().then(accessToken => {console.log("at ()app:",accessToken);return logIn()})
      window.location.href = authUri;
      this.setState({ isLoggedIn: !this.state.isLoggedIn });
      this.mystorage.setItem('isLoggedIn', true);
      
      
    }

    console.log('sessionStorage.getItem("isLoggedIn") :', data[0]);
    
  }
  render() {
    return (
      <Router>
        <div className={"main-container"}>
           <div>
             Navigation
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/login">{(this.state.isLoggedIn)?"Log Out":"Log In"}</Link>
              </li>
            </ul>
          </div>
          <Switch>
            <Route
              path="/login"
              component={() => (
                <Login login={this.login} isLogged={this.isLoggedIn()} />
              )}
            />
            <PrivateRoute
              exact
              path="/"
              component={Home}
              isAuthenticated={!!this.isLoggedIn()}
              redirect="/login"
            />
            <Route component={NotFound} />
          </Switch>
        </div>
      </Router>
    );
  }
}

// const rootElement = document.getElementById("root");
// ReactDOM.render(<App />, rootElement);
